package dei.uc.pt.ar.paj.adomain.facade;

import javax.ejb.Local;

import dei.uc.pt.ar.paj.adomain.entities.SimpleUser;


public interface ISimpleUserFacade extends IEntityFacade<SimpleUser>{

	// Add Custom Queries here
	
}
